package com.devcamp.j6040.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.j6040.models.Car;

public interface ICarRepository extends JpaRepository<Car, Long> {
    Car findByCarcode(String car);

}