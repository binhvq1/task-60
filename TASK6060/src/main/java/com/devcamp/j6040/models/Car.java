package com.devcamp.j6040.models;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "cars")
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int car_id;

    @Column(name = "carcode")
    private String carcode;

    @Column(name = "carname")
    private String carname;

    @OneToMany(mappedBy = "car", cascade = CascadeType.ALL)
    @JsonManagedReference
    private Set<CarType> types;

    public Set<CarType> getTypes() {
        return types;
    }

    public void setTypes(Set<CarType> types) {
        this.types = types;
    }

    public String getCarcode() {
        return carcode;
    }

    public void setCarcode(String carcode) {
        this.carcode = carcode;
    }

    public String getCarname() {
        return carname;
    }

    public void setCarname(String carname) {
        this.carname = carname;
    }

    public Car() {
    }

    public int getCar_id() {
        return car_id;
    }

    public void setCar_id(int car_id) {
        this.car_id = car_id;
    }

    public Car(int car_id, String carcode, String carname, Set<CarType> types) {
        this.car_id = car_id;
        this.carcode = carcode;
        this.carname = carname;
        this.types = types;
    }

}
