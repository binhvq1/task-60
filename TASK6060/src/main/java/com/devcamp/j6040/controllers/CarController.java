package com.devcamp.j6040.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.j6040.models.Car;
import com.devcamp.j6040.models.CarType;
import com.devcamp.j6040.repositories.ICarRepository;
import com.devcamp.j6040.repositories.ICarTypeRepository;

@RestController
@CrossOrigin
@RequestMapping
public class CarController {
    @Autowired
    ICarRepository carRepository;
    ICarTypeRepository carTypeRepository;

    @GetMapping("/devcamp_cars")
    public ResponseEntity<List<Car>> getAllCars() {
        try {
            List<Car> pCars = new ArrayList<Car>();
            carRepository.findAll().forEach(pCars::add);
            return new ResponseEntity<>(pCars, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/devcamp-cartypes")
    public ResponseEntity<Set<CarType>> getAllCarTypes(@RequestParam(value = "carcode") String carCode) {
        try {
            Car vCar = carRepository.findByCarcode(carCode);
            if (vCar != null) {
                return new ResponseEntity<>(vCar.getTypes(), HttpStatus.OK);

            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }
}
