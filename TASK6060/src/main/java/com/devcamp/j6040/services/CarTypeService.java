package com.devcamp.j6040.services;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.j6040.models.CarType;
import com.devcamp.j6040.repositories.ICarTypeRepository;

@Service
public class CarTypeService {
    @Autowired
    ICarTypeRepository carTypeRepository;

    public ArrayList<CarType> getAllCars() {
        ArrayList<CarType> listCarTypes = new ArrayList<>();
        carTypeRepository.findAll().forEach(listCarTypes::add);
        return listCarTypes;
    }

}