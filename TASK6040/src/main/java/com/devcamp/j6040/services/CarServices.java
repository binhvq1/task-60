package com.devcamp.j6040.services;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcamp.j6040.models.Car;
import com.devcamp.j6040.repositories.ICarRepository;

@Service
public class CarServices {
    @Autowired
    ICarRepository carRepository;

    public ArrayList<Car> getAllCars() {
        ArrayList<Car> listCars = new ArrayList<>();
        carRepository.findAll().forEach(listCars::add);
        return listCars;
    }

}
