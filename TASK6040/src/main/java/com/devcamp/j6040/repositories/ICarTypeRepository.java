package com.devcamp.j6040.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.j6040.models.CarType;

public interface ICarTypeRepository extends JpaRepository<CarType, Long> {
    // CarType findByCarTypeId(String id);
}
